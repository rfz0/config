#
# ~/.profile
#

export EDITOR=vim
export LESS=-R
export LESSOPEN='| pygmentize -g %s'

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
